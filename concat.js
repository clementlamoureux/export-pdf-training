const PDFMerge = require('pdf-merge');
const fs = require('fs');

fs.readdir(`${__dirname}/trainings/`, (err, files) => {
  console.log(files);
  files = files
    .filter((a) => a.indexOf('.html') === -1)
    .map((a) => `${__dirname}/trainings/` + a)
  PDFMerge(files, {output: `${__dirname}/trainings/all.pdf`});
})

