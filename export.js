const https = require('https');
const fs = require('fs');
const pdf = require('html-pdf');
const request = require('request');

exports.trainingPdf = () => {
  https.get('https://training.zenika.com/api/api/trainings', (res) => {
    console.log(res);
      let data = '';
      res.on('data', (d) => {
        data += d;
      });
      res.on('end', () => {
        data = JSON.parse(data);
        data.forEach((d) => {
          var content = "";
          let outline = d.outline;
          let filePathHtml = __dirname + '/trainings/' + outline + '.html';
          let filePathPdf = __dirname + '/trainings/' + outline + '.pdf';
          console.log('Downloading ' + outline + '.html from zenika.com');
          let file = fs.createWriteStream(filePathPdf);
          let a = request({
            url: 'https://platon.zenika.com/api/public/trainings/doc/' + outline + '.pdf',
            encoding: 'latin1'
          }, (error, response, body) => {
            // console.log('Downloaded ' + outline + '.html from zenika.com');
            // body = body.replace(/(\/root\/prj\/platon\/src\/main\/resources\/static\/images\/logos\/)/g, 'file://' + __dirname + '/assets/')
            // body = body.replace('<body>', '<body style="width: 80%;margin:auto;font-size:0.75em">')
            file.write(body);
            // setTimeout(() => {
            //   console.log('Generating PDF at ' + filePathPdf);
            //   let html = fs.readFileSync(filePathHtml, 'utf8');
            //   let options = {
            //     format: 'A4',
            //     orientation: 'portrait',
            //     border: '0',
            //     zoomFactor: "0.4",
            //     header:{
            //       height: '0'
            //     }
            //   };
            //   pdf.create(html, options).toFile(filePathPdf, function (err, res) {
            //     if (err) return console.log(err);
            //     console.log('✓ GENERATED ' + filePathHtml); // { filename: '/app/businesscard.pdf' }
            //   });
            // }, 2000);
          });
          a.on('data', function (response) {
            // console.log(outline);
            // file.write(data.toString());
            // console.log('Downloaded ' + outline + '.html from zenika.com');
            // setTimeout(() => {
            //   console.log('Generating PDF from ' + outline + '.html');
            //   let html = fs.readFileSync(filePathHtml, 'utf8');
            //   let options = {format: 'A4', orientation: 'landscape'};
            //   pdf.create(html, options).toFile(filePathPdf, function (err, res) {
            //     if (err) return console.log(err);
            //     console.log('✓ GENERATED ' + filePathHtml); // { filename: '/app/businesscard.pdf' }
            //   });
            // }, 2000);
          });
        });
      });
    }
  ).on('error', (e) => {
    console.error(e);
  });
}
;

exports.trainingPdf();